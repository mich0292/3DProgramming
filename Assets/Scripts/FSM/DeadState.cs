﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadState : State
{
    private Player_FSM player;
    private StateMachine stateMachine;

    public DeadState(Player_FSM player_script, StateMachine _stateMachine)
    {
        player = player_script;
        stateMachine = _stateMachine;
    }

    //How to enter the state
    public override void Enter()
    {
        base.Enter(); //Call the method Enter from State class https://www.geeksforgeeks.org/c-sharp-method-overriding/
        player.enemyCollisionCheck.enabled = false;
        player.animator.Play("Fox_Falling");
    }

    //HandleInput, LogicUpdate, PhysicsUpdate define the update loop
    public override void HandleInput()
    {
        base.HandleInput();
    }

    public override void LogicUpdate()
    {
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override void Exit()
    {
        base.Exit();
        //Debug.Log("Exiting Idle State");
    }
}
