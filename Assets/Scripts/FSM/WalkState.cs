﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkState : State
{
    private Player_FSM player;
    private StateMachine stateMachine;
    private float inputH;
    private float inputV;
    private bool jumped;
    private bool isGrounded;
    private bool isDead;
    private bool attackInput;

    public WalkState(Player_FSM _player, StateMachine _stateMachine)
    {
        player = _player;
        stateMachine = _stateMachine;
    }

    //How to enter the state
    public override void Enter()
    {
        base.Enter();
        isDead = false;
        //Debug.Log("Entering Walk State");
    }

    //HandleInput, LogicUpdate, PhysicsUpdate define the update loop
    public override void HandleInput()
    {
        base.HandleInput();
        inputH = Input.GetAxis("Horizontal");
        inputV = Input.GetAxis("Vertical");
        jumped = Input.GetButtonDown("Jump");
        attackInput = Input.GetButtonDown("Fire1");
    }

    public override void LogicUpdate() 
    {
        player.animator.SetFloat("inputH", inputH);
        player.animator.SetFloat("inputV", inputV);

        if (SoundManager.Instance)
            SoundManager.Instance.Play(SoundManager.Instance.walk);

        if (isDead)
        {
            GameManager.Instance.Lose();
            stateMachine.ChangeState(player.dead);
        }
        else if (isGrounded)
        {
            if (jumped)
                stateMachine.ChangeState(player.jump);
            if (inputH == 0.0f && inputV == 0.0f)
                stateMachine.ChangeState(player.idle);
            if (attackInput)
                stateMachine.ChangeState(player.attack);
        }
        else if (!isGrounded && jumped)
            stateMachine.ChangeState(player.doubleJump);        
    }

    public override void PhysicsUpdate() 
    {
        base.PhysicsUpdate();
        isDead = player.CheckCollisionWater();
        isDead |= player.CheckHealth();
        isGrounded = player.CheckCollisionOverlap();
        player.Move(inputH, inputV);
    }

    public override void Exit()
    {
       // if (SoundManager.Instance)
            //SoundManager.Instance.Stop(SoundManager.Instance.walk);
        //Debug.Log("Exiting Walk State");
    }
}
