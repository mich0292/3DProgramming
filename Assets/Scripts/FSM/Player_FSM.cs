﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region References
//https://itnext.io/how-to-write-a-simple-3d-character-controller-in-unity-1a07b954a4ca?gi=6f6e530e71e7
//https://answers.unity.com/questions/334708/gravity-with-character-controller.html
//https://www.raywenderlich.com/6034380-state-pattern-using-unity
#endregion

public class Player_FSM : MonoBehaviour
{
    #region FSM
    [HideInInspector] public StateMachine movementSM;
    [HideInInspector] public IdleState idle;
    [HideInInspector] public WalkState walk;
    [HideInInspector] public RunState run;
    [HideInInspector] public JumpState jump;
    [HideInInspector] public DoubleJumpState doubleJump;
    [HideInInspector] public AttackState attack;
    [HideInInspector] public DeadState dead;
    #endregion

    #region Movement
    [SerializeField] private float speed;
    [SerializeField] private float jumpForce; 
    //public GameObject leg;
    private Quaternion oriRotation;
    //private Quaternion legOriRotation;
    private bool canDoubleJump = true; 
    private bool isGrounded; //https://answers.unity.com/questions/196381/how-do-i-check-if-my-rigidbody-player-is-grounded.html
    #endregion

    #region Animation, Camera, Rigidbody, Colliders
    //Camera and rotation
    public float camSpeed;
    //Animator
    public Animator animator;
    //Rigidbody
    private Rigidbody rigidbody;
    //Colliders
    public BoxCollider enemyCollisionCheck { get; private set; }
    private CapsuleCollider[] legColliderList; //specifically for legs (movement)
    [SerializeField] private CapsuleCollider frontLeftLeg;
    [SerializeField] private CapsuleCollider frontRightLeg;
    [SerializeField] private CapsuleCollider backLeftLeg;
    [SerializeField] private CapsuleCollider backRightLeg;
    #endregion

    #region Attack
    [SerializeField] private SphereCollider tailCollider; //specifically for tail part (attack)
    private Vector3 colliderCenter;
    [SerializeField] private int damageOutput;
    #endregion

    #region MonoBehavior Callbacks
    void Awake()
    {
        movementSM = new StateMachine();

        idle = new IdleState(this, movementSM);
        walk = new WalkState(this, movementSM);
        run = new RunState(this, movementSM);
        jump = new JumpState(this, movementSM);
        //Why jump & doubleJump? To fix landing sound
        doubleJump = new DoubleJumpState(this, movementSM);
        attack = new AttackState(this, movementSM);
        dead = new DeadState(this, movementSM);      
        
        movementSM.Initialize(idle);

        legColliderList = new CapsuleCollider[4] { frontLeftLeg, frontRightLeg, backLeftLeg, backRightLeg };
        //tailCollider.center is in local position, need to translate to world position for Physics raycast to work
        colliderCenter = transform.TransformPoint(tailCollider.center);
        enemyCollisionCheck = GetComponentInChildren<BoxCollider>();
        tailCollider.enabled = false;

        oriRotation = transform.rotation;
        rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();        
    }

    void Update()
    {
        if (Time.timeScale == 0)
            return;

        movementSM.CurrentState.HandleInput();
        movementSM.CurrentState.LogicUpdate();
    }

    void FixedUpdate()
    {
        movementSM.CurrentState.PhysicsUpdate();
    }

    #endregion

    //For isGrounded check
    public bool CheckCollisionOverlap()
    {
        //https://answers.unity.com/questions/8715/how-do-i-use-layermasks.html
        int GroundMask = 1 << 9;
        int MovingPlatformMask = 1 << 10;
        int CrateMask = 1 << 11;

        int CombinedMask = GroundMask | MovingPlatformMask | CrateMask;

        //Do note it checks the isGrounded from the fox's tail too
        foreach (CapsuleCollider cap in legColliderList)
        {
            Debug.DrawRay(cap.bounds.center, -Vector3.up * (cap.bounds.extents.y + 0.10f), Color.white);
            //LayerMask mask = LayerMask.GetMask("Ground");
            //return Physics.Raycast(transform.position, -Vector3.up, distToGround - 0.9f  + 0.1f); //disToGround + etc = max distance
            if (Physics.Raycast(cap.bounds.center, -Vector3.up, cap.bounds.extents.y + 0.10f, CombinedMask))
                return true;
        }
        return false;
    }

    //To check against water = instant death
    public bool CheckCollisionWater()
    {
        //Do note it checks the isGrounded from the fox's tail too
        foreach (CapsuleCollider cap in legColliderList)
        {
            //return Physics.Raycast(transform.position, -Vector3.up, distToGround - 0.9f  + 0.1f); //disToGround + etc = max distance
            // 1 << 4 = Water Layer
            if (Physics.Raycast(cap.bounds.center, -Vector3.up, cap.bounds.extents.y + 0.10f, 1 << 4))
                return true;
        }
        return false;
    }

    public bool CheckHealth()
    { 
        return GameManager.Instance.PlayerDie(); 
    }

    public void Move(float horizontalMove, float verticalMove)
    {
        Vector3 eulerAngle = transform.eulerAngles;
        Quaternion rotate;

        //reference from: https://forum.unity.com/threads/moving-a-rigidbody-at-a-constant-speed.76470/
        Vector3 move = transform.forward * verticalMove + transform.right * horizontalMove;
        if (rigidbody.velocity.magnitude < speed)
            rigidbody.AddForce(move * speed);

        eulerAngle.y += 1.0f * horizontalMove;
        rotate = Quaternion.Euler(eulerAngle);
        transform.rotation = Quaternion.Lerp(rotate, transform.rotation, camSpeed * Time.deltaTime);
        transform.rotation = Quaternion.Euler(new Vector3(oriRotation.eulerAngles.x, transform.eulerAngles.y, oriRotation.eulerAngles.z));
    }

    public void Jump()
    {
        rigidbody.AddForce(Vector3.up * jumpForce);  
    }

    public void DoubleJump()
    {
        rigidbody.AddForce(Vector3.up * jumpForce * 1.8f);
    }

    public void CheckTailCollision()
    {
        //check collision & reduce the enemy's health or destroy things
        //Check which enemy is within collision > enemy List? or smth else?
        //LayerMask enemyLayers
        //Physics.OverlapSphere
        tailCollider.enabled = true;
        int CrateMask = 1 << 11;
        int EnemyMask = 1 << 12;

        int CombinedMask = CrateMask | EnemyMask;

        //https://www.youtube.com/watch?v=sPiVz1k-fEs 
        Collider[] hitColliders = Physics.OverlapSphere(tailCollider.gameObject.transform.position, tailCollider.radius, CombinedMask);
        Debug.Log(hitColliders.Length);        
       
        foreach (var collider in hitColliders)
        {
            if (!collider.isTrigger)
            {
                Debug.Log(collider.gameObject);
                if (collider.gameObject.GetComponent<HealthScript>())
                    collider.gameObject.GetComponent<HealthScript>().Hit(damageOutput);
                else
                    collider.gameObject.GetComponentInParent<HealthScript>().Hit(damageOutput);       
            }
        }

        tailCollider.enabled = false;
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        //Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
        //Gizmos.DrawWireSphere(tailCollider.center, tailCollider.radius);
        if (tailCollider.enabled)
            Gizmos.DrawWireSphere(tailCollider.gameObject.transform.position, tailCollider.radius);
    }

}
