﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : State
{
    private Player_FSM player;
    private StateMachine stateMachine;
    private float inputH;
    private float inputV;
    private bool isGrounded;
    private bool isDead;
    private bool jumped;
    private bool attackInput;

    public IdleState(Player_FSM player_script, StateMachine _stateMachine) 
    {
        player = player_script;
        stateMachine = _stateMachine;
    }

    //How to enter the state
    public override void Enter()
    {
        base.Enter(); //Call the method Enter from State class https://www.geeksforgeeks.org/c-sharp-method-overriding/
        //When the character isn't moving
        //Debug.Log("Entering Idle State");
        isDead = false;
        inputH = inputV = 0.0f;
    }

    //HandleInput, LogicUpdate, PhysicsUpdate define the update loop
    public override void HandleInput()
    {
        base.HandleInput();
        inputH = Input.GetAxis("Horizontal");
        inputV = Input.GetAxis("Vertical");
        jumped = Input.GetButtonDown("Jump");
        attackInput = Input.GetButtonDown("Fire1");
    }

    public override void LogicUpdate() 
    {
        player.animator.Play("Fox Idle");

        if (isDead)
        {
            GameManager.Instance.Lose();
            stateMachine.ChangeState(player.dead);
        }
        else if (isGrounded)
        {
            if (jumped)
                stateMachine.ChangeState(player.jump);
            if (inputH > 0.1f || inputH < -0.1f || inputV > 0.1f || inputV < -0.1)
                stateMachine.ChangeState(player.walk);
            if (attackInput)
                stateMachine.ChangeState(player.attack);
        }    
        else if (!isGrounded && jumped)
            stateMachine.ChangeState(player.doubleJump);        
    } 
    
    public override void PhysicsUpdate() 
    {
        base.PhysicsUpdate();
        isDead = player.CheckCollisionWater();
        isDead |= player.CheckHealth();
        isGrounded = player.CheckCollisionOverlap();        
        player.Move(inputH, inputV);
    } 

    public override void Exit() 
    {
        base.Exit();
        //Debug.Log("Exiting Idle State");
    }
}
