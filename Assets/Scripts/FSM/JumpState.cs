﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpState : State
{
    private Player_FSM player;
    private StateMachine stateMachine;
    private float inputH;
    private float inputV;
    private bool isGrounded;
    private bool isDead;
    private bool canDoubleJump;
    private bool jumped;

    public JumpState(Player_FSM _player, StateMachine _stateMachine)
    {
        player = _player;
        stateMachine = _stateMachine;
    }

    //How to enter the state
    public override void Enter()
    {
        base.Enter();
        isGrounded = false;
        isDead = false;
        canDoubleJump = true;
        player.animator.Play("Fox_Jump");
        player.Jump();
        //Debug.Log("Entering Jump State");
        if (SoundManager.Instance)
            SoundManager.Instance.Play(SoundManager.Instance.jump);
    }

    //HandleInput, LogicUpdate, PhysicsUpdate define the update loop
    public override void HandleInput()
    {
        base.HandleInput();
        inputH = Input.GetAxis("Horizontal");
        inputV = Input.GetAxis("Vertical");
        jumped = Input.GetButtonDown("Jump");
        //Debug.Log("Jump pressed = " + jumped);
    }

    public override void LogicUpdate() 
    {
        base.LogicUpdate();
        if (isDead)
        {
            GameManager.Instance.Lose();
            stateMachine.ChangeState(player.dead);
        }
        else if (isGrounded) 
        {
            if (inputH > 0.1f || inputH < -0.1f || inputV > 0.1f || inputV < -0.1f)
                stateMachine.ChangeState(player.walk);
            else
                stateMachine.ChangeState(player.idle);                
        }
        else if (jumped && canDoubleJump) //I think this can be deleted, not sure;
        {
            stateMachine.ChangeState(player.doubleJump);
        }
        else
        {
            //When the player hasn't touched the ground, the player can move in midair
            if (inputH > 0.1f || inputH < -0.1f || inputV > 0.1f || inputV < -0.1)
                player.Move(inputH, inputV);
        }

    }  
    
    public override void PhysicsUpdate() 
    {
        base.PhysicsUpdate();
        isDead = player.CheckCollisionWater();
        isDead |= player.CheckHealth();
        isGrounded = player.CheckCollisionOverlap();
    }

    public override void Exit() 
    {
        //Debug.Log("Exiting Jump State");
        //player.animator.ResetTrigger("Jump");
        //SoundManager.Instance.Stop(SoundManager.Instance.jump);
    }
}
