﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleJumpState : State
{
    private Player_FSM player;
    private StateMachine stateMachine;
    private float inputH;
    private float inputV;
    private bool isGrounded;
    private bool isDead;

    public DoubleJumpState(Player_FSM _player, StateMachine _stateMachine)
    {
        player = _player;
        stateMachine = _stateMachine;
    }

    //How to enter the state
    public override void Enter()
    {
        base.Enter();
        isGrounded = false;
        isDead = false;
        player.DoubleJump();
        player.animator.Play("Fox_Somersault_InPlace");
        Debug.Log("Entering DoubleJump State");
        if (SoundManager.Instance)
            SoundManager.Instance.Play(SoundManager.Instance.jump);
    }

    //HandleInput, LogicUpdate, PhysicsUpdate define the update loop
    public override void HandleInput()
    {
        base.HandleInput();
        inputH = Input.GetAxis("Horizontal");
        inputV = Input.GetAxis("Vertical");
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (isDead)
        {
            GameManager.Instance.Lose();
            stateMachine.ChangeState(player.dead);
        }
        else if (isGrounded)
        {
            if (inputH > 0.1f || inputH < -0.1f || inputV > 0.1f || inputV < -0.1f)
                stateMachine.ChangeState(player.walk);
            else
                stateMachine.ChangeState(player.idle);
        }
        else {
            //When the player hasn't touched the ground, the player can move in midair
            if (inputH > 0.1f || inputH < -0.1f || inputV > 0.1f || inputV < -0.1)
                player.Move(inputH, inputV);
        }

        //isGrounded = Physics.Raycast(_player.GetComponent<Transform>().position, -Vector3.up, distToGround + 0.1);
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
        isDead = player.CheckCollisionWater();
        isDead |= player.CheckHealth();
        isGrounded = player.CheckCollisionOverlap();
    }

    public override void Exit()
    {
        Debug.Log("Exiting DoubleJump State");
    }
}
