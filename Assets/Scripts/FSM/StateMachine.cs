﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//https://www.raywenderlich.com/6034380-state-pattern-using-unity
public class StateMachine 
{
    public State CurrentState;

    //first state
    public void Initialize(State state)
    {
        CurrentState = state;
        state.Enter();
    }

    //handles transition between states
    public void ChangeState(State newState)
    {
        CurrentState.Exit();

        CurrentState = newState;
        newState.Enter();
    }
}
