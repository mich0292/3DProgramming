﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : State
{
    private Player_FSM player;
    private StateMachine stateMachine;
    private float inputH;
    private float inputV;
    private bool isDead;

    public AttackState(Player_FSM _player, StateMachine _stateMachine)
    {
        player = _player;
        stateMachine = _stateMachine;
    }

    //How to enter the state
    public override void Enter()
    {
        base.Enter();
        isDead = false;
        player.animator.Play("Fox_Attack_Tail");
        Debug.Log("Entering Attack State");
        //Freeze rigidbody to prevent being pushed
        player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        //SoundManager.Instance.Play(SoundManager.Instance.jump);
    }

    //HandleInput, LogicUpdate, PhysicsUpdate define the update loop
    public override void HandleInput()
    {
        base.HandleInput();
        inputH = Input.GetAxis("Horizontal");
        inputV = Input.GetAxis("Vertical");
    }

    public override void LogicUpdate() 
    {
        base.LogicUpdate();

        if (isDead)
        {
            GameManager.Instance.Lose();
            stateMachine.ChangeState(player.dead);
        }
        //Check that animation play finish then change state, if not collision cannot check at all
        else if (!player.animator.GetCurrentAnimatorStateInfo(0).IsName("Fox_Attack_Tail")) 
        {
            if (inputH > 0.1f || inputH < -0.1f || inputV > 0.1f || inputV < -0.1)
                stateMachine.ChangeState(player.walk);
            if (inputH == 0.0f && inputV == 0.0f)
                stateMachine.ChangeState(player.idle);
        }
    }

    public override void PhysicsUpdate() 
    {
        base.PhysicsUpdate();
        isDead = player.CheckCollisionWater();
        isDead |= player.CheckHealth();
        player.CheckTailCollision();
    }

    public override void Exit()
    {
        base.Exit();
        Debug.Log("Exiting Attack State");
        //Unfreeze rigidbody to the original constraints. Note: Y = so that player won't move up the crates
        player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
    }
}
