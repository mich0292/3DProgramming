﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunState : State
{
    private float inputH;
    private float inputV;
    private Player_FSM player;
    private StateMachine stateMachine;

    public RunState(Player_FSM _player, StateMachine _stateMachine)
    {
        player = _player;
        stateMachine = _stateMachine;
    }

    //How to enter the state
    public override void Enter()
    {
        base.Enter();
        //When the character isn't moving
        Debug.Log("Entering Run State");
        inputH = inputV = 0.0f;
    }

    //HandleInput, LogicUpdate, PhysicsUpdate define the update loop
    public override void HandleInput()
    {
        base.HandleInput();
        inputH = Input.GetAxis("Horizontal");
        inputV = Input.GetAxis("Vertical");
    }

    public override void LogicUpdate() 
    {
        //player.TriggerAnimation(this.GetType().Name);
    }

    public override void PhysicsUpdate() { }
    public override void Exit()
    {
        Debug.Log("Exiting Run State");
    }
}
