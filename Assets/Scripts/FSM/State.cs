﻿
public abstract class State
{
    //How to enter the state
    public virtual void Enter() {}
    //HandleInput, LogicUpdate, PhysicsUpdate define the update loop
    public virtual void HandleInput() { }
    public virtual void LogicUpdate() { }   //core logic
    public virtual void PhysicsUpdate() { } //physics logic and calculation
    //When to exit the state
    public virtual void Exit() { }
}
