﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckPointManager : MonoBehaviour
{
    public static CheckPointManager Instance;
    public bool crossCheckPoint { get; set; }

    // Start is called before the first frame update
    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(this.gameObject);

        crossCheckPoint = false;
    }

    public void RespawnPlayer()
    {
        if (crossCheckPoint)
        {           
            GameObject player = GameObject.FindWithTag("Player");
            GameObject respawn = GameObject.FindWithTag("Respawn");
            player.transform.position = respawn.transform.position;
            player.transform.rotation = respawn.transform.rotation;
        }
    }
}
