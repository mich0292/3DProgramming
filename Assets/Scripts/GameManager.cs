﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    //public variable
    public static GameManager Instance;
    private SceneManagement sceneManager;
    [SerializeField] GameObject TransitionManager;
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] TextMeshProUGUI healthText;
    [SerializeField] TextMeshProUGUI SaveMessage;
    [SerializeField] Canvas loseCanvas;
    [SerializeField] Canvas winCanvas;
    [SerializeField] Canvas pauseCanvas;
    
    private int health;
    private const int MAX_HEALTH = 5;
    private int score;
    private bool isPlaying;

    public void Awake()
    {
        Instance = this;
        if (SaveLoadSystem.gameData != null) // && SceneManager.GetActiveScene().name.Contains(SaveLoadSystem.gameData.level.ToString())
        {
            Debug.Log("Active scene: " + SceneManager.GetActiveScene().name);
            health = SaveLoadSystem.gameData.health;
            score = SaveLoadSystem.gameData.score;

            SaveLoadSystem.gameData = null; //to prevent next new level load previous value
        }
        else
        {
            score = 0;
            health = MAX_HEALTH;
        }

        isPlaying = true;      
        if (scoreText)
            scoreText.text = "Score: " + this.score;
        if (healthText)
            healthText.text = "Health: " + this.health;
        sceneManager = TransitionManager.GetComponent<SceneManagement>();
    
        SaveMessage.maxVisibleCharacters = 0;        
    }

    void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex > 0 && CheckPointManager.Instance.crossCheckPoint)
        {
            CheckPointManager.Instance.RespawnPlayer();
            GameData playerData = SaveLoadSystem.LoadGame();
            this.score = playerData.score;
            this.health = playerData.health;
            SaveLoadSystem.gameData = null;
            RefreshUI();
        }
    }

    public void Update()
    {
        //Make sure the pause only works on levels and not main menu
        if (SceneManager.GetActiveScene().buildIndex > 0 && Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPlaying) Pause();
            else unPause();
        }
    }

    public void IncreaseScore(int score)
    {
        this.score += score;
        scoreText.text = "Score: " + this.score;
        RefreshUI();
    }

    public void ReduceHealth(int damage)
    {
        this.health -= damage;

        if (health < 0)
            health = 0;

        RefreshUI();
    }

    public void IncreaseHealth()
    {
        if (this.health < MAX_HEALTH)
            this.health += 1;
        RefreshUI();
    }

    public bool PlayerDie() => health <= 0;

    public void RefreshUI()
    {
        scoreText.text = "Score: " + this.score;
        healthText.text = "Health: " + this.health;
    }

    //Lose - display UI, restart that level(?) or return to main menu? 
    //Eventually need save and load game 
    public void Lose()
    {
        loseCanvas.enabled = true;
        Invoke("Pause2", 0.5f);
    }

    //Win - display UI, advance to next level
    public void Win()
    {
        winCanvas.enabled = true;
        CheckPointManager.Instance.crossCheckPoint = false;
        Invoke("Pause2", 0.5f);
    }

    public void Pause2()
    {
        Time.timeScale = 0.0f;
    }

    public void Pause()
    {
        Time.timeScale = 0.0f;
        pauseCanvas.enabled = true;
        isPlaying = false;
    }

    public void unPause()
    {
        Time.timeScale = 1.0f;
        pauseCanvas.enabled = false;
        isPlaying = true;
    }

    //Not needed because GameManager isn't Dont Destroy on Load
    public void Restart()
    {
        loseCanvas.enabled = false;
        winCanvas.enabled = false;
        this.score = 0;
        this.health = 5;
        RefreshUI();
    }

    public void SaveGame()
    {
        int levelIndex = SceneManager.GetActiveScene().buildIndex;
        SaveLoadSystem.SaveGame(health, score, levelIndex);
        SaveMessage.GetComponent<TextWriterEffect>().Play();
        Debug.Log(levelIndex);     
    }

    public void LoadGame()
    {
        GameData playerData = SaveLoadSystem.LoadGame();
        Debug.Log(playerData.level);
        switch (playerData.level)
        {
            case 1: sceneManager.LoadLevel1(); break;
            case 2: sceneManager.LoadLevel2(); break;
        }
        this.score = playerData.score;
        this.health = playerData.health;
    }
}
