﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour
{
    //https://forum.unity.com/threads/noob-series-one-help-with-health-system.244980/#post-1621402
    public int health; 

    private GameObject parent;

    public void Hit(int amount)
    {
        health -= amount;

        if (gameObject.tag == "Enemy")
        {
            gameObject.GetComponent<Enemy>().PlayAnimation();
        }

        if (health <= 0)
            Die();

    }

    public void Die() 
    {
        //Debug.Log(gameObject.GetType().ToString());
        switch (gameObject.tag)
        {
            case "BigCrate":
                parent = this.transform.parent.gameObject;
                parent.GetComponent<Destructible>().DestroyCrate(); 
                break;
            case "SmallCrate":
                GetComponent<Destructible>().DestroyCrate(); 
                break;   
        }

        if (gameObject.tag == "Enemy")
        {
            gameObject.GetComponent<Enemy>().Die();
        }          

    }
    
}
