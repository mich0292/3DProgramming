﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : MonoBehaviour
{
    //Fade in & out reference: https://www.youtube.com/watch?v=Oadq-IrOazg
    public Animator animator;
    public Slider SFXSlider;
    public Slider BGMSlider;
    public GameObject settingsCanvas;
    public GameObject winCanvas;
    public GameObject endCanvas;
    private string levelToLoad;

    public void LoadMainMenu()
    {
        Debug.Log("Click here");
        Time.timeScale = 1f;
        levelToLoad = "Home";
        animator.SetTrigger("FadeOut");
        CheckPointManager.Instance.crossCheckPoint = false;
    }

    public void LoadLevel1()
    {
        Time.timeScale = 1f;
        levelToLoad = "Level1";
        animator.SetTrigger("FadeOut");
    }

    public void LoadLevel2()
    {
        Time.timeScale = 1f;
        levelToLoad = "Level2";
        animator.SetTrigger("FadeOut");
    }

    public void LoadSettings()
    {
        Time.timeScale = 1f;
        if (SoundManager.Instance)
        {
            BGMSlider.value = SoundManager.Instance.BGMGetVolume();
            SFXSlider.value = SoundManager.Instance.SFXGetVolume();
        }
        settingsCanvas.SetActive(true);
    }

    public void OpenEndCanvas()
    {
        winCanvas.SetActive(false);
        endCanvas.SetActive(true);
    }

    public void CloseEndCanvas()
    {
        winCanvas.SetActive(true);
        endCanvas.SetActive(false);
    }

    public void Save()
    {
        if (SoundManager.Instance)
        {
            SoundManager.Instance.BGMSetVolume(BGMSlider.value);
            SoundManager.Instance.SFXSetVolume(SFXSlider.value);
        }
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void OnFadeCompleted()
    {
        Debug.Log("Fade Complete");
        SceneManager.LoadScene(levelToLoad);
    }

}
