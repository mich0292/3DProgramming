﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [HideInInspector]
    public Vector3 direction;
    public float bulletForce;
    [HideInInspector]
    public GameObject enemyParent;

    private Rigidbody rigidbody;
    private Collider foxContactPoint;
    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        //rigidbody = GetComponent<Rigidbody>();
        foxContactPoint = player.GetComponent<Player_FSM>().enemyCollisionCheck;
        Destroy(gameObject, 3.0f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = transform.position + direction * bulletForce * Time.deltaTime;
        //rigidbody.AddForce(direction * bulletForce);
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == enemyParent)
            return;
        if(collision.collider == foxContactPoint)
        {
            Debug.Log("Hit");
            GameManager.Instance.ReduceHealth(enemyParent.GetComponent<Enemy>().damage);
        }
        Destroy(gameObject);
    }
}
