﻿using UnityEngine;

public class Platform : MonoBehaviour
{
    public GameObject player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    public void OnCollisionStay(Collision collision)
    {
        if(collision.gameObject == player)
        {
            player.transform.parent = transform;
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        if(collision.gameObject == player) 
        {
           player.transform.parent = null;
        }
    }
}
