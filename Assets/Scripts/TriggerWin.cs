﻿using UnityEngine;

public class TriggerWin : MonoBehaviour
{
    private bool called = false;

    //Use to call the Win() in Game Manager script
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "PlayerCollision" && !called)
        {
            GameManager.Instance.Win();
            called = true;
        }
    }
}
