﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveLoadSystem
{
    public static GameData gameData = null;

    public static void SaveGame(int health, int score, int level)
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/GameData.txt";

        FileStream fileStream = new FileStream(path, FileMode.Create);

        GameData data = new GameData(health, score, level);

        binaryFormatter.Serialize(fileStream, data);
        fileStream.Close();
    }

    public static GameData LoadGame()
    {
        string path = Application.persistentDataPath + "/GameData.txt";

        if(File.Exists(path))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream fileStream = new FileStream(path, FileMode.Open);

            gameData = binaryFormatter.Deserialize(fileStream) as GameData;
            fileStream.Close();
            return gameData;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
}


