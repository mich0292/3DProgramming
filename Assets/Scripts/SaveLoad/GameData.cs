﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//reference from: https://www.youtube.com/watch?v=XOjd_qU2Ido
[System.Serializable]
public class GameData
{
    public int health;
    public int score;
    public int level;

    public GameData(int health, int score, int level)
    {
        this.health = health;
        this.score = score;
        this.level = level;
    }

}
