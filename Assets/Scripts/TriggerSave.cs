﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSave : MonoBehaviour
{
    // Start is called before the first frame update
    void OnTriggerEnter()
    {
        if (!CheckPointManager.Instance.crossCheckPoint)
        {
            Debug.Log(CheckPointManager.Instance.crossCheckPoint);
            GameManager.Instance.SaveGame();
            CheckPointManager.Instance.crossCheckPoint = true;
        }            
    }
}

