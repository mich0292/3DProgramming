﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public enum FruitType { Apple, Pear, Grape, Banana, Strawberry};
    public FruitType fruitType;
    private int score;

    public void Start()
    {
        if (this.tag == "Fruit")
        {
            switch (fruitType)
            {
                case FruitType.Apple: score = 500; break;
                case FruitType.Pear: score = 400; break;
                case FruitType.Grape: score = 300; break;
                case FruitType.Banana: score = 200; break;
                case FruitType.Strawberry: score = 100; break;
            }
        }
        else score = 0;
    }

    public void OnTriggerEnter(Collider collision)
    {
        //Ensure only collision with player will remove the fruit and add score
        if (collision.gameObject.tag == "PlayerCollision")
        {
            if (this.tag == "Fruit")
            {
                GameManager.Instance.IncreaseScore(score);
                Destroy(gameObject);
            }
            else if (this.tag == "Health")
            {
                GameManager.Instance.IncreaseHealth();
                Destroy(gameObject);
            }        
        }       
    }
}
