﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject cam;
    [Tooltip("Max rotation for left right")]
    public float rotationLeftRight;
    [Tooltip("Max rotation for top")]
    public float rotationTop;
    public float speed;

    private Quaternion oriRotation;
    private Quaternion camOriRotation;
    // Start is called before the first frame update
    void Start()
    {
        oriRotation = transform.rotation;
        camOriRotation = cam.transform.rotation;
    }

    /// <summary>
    /// Reference from: https://answers.unity.com/questions/127765/how-to-restrict-quaternionslerp-to-the-y-axis.html
    /// </summary>
    // Update is called once per frame
    void Update()
    {
        Vector3 eulerAngle = transform.eulerAngles;
        Quaternion rotate;

        if (Input.GetButton("CameraLeft"))
        {
            eulerAngle.y -= 10f;

            if(Mathf.Abs(eulerAngle.y) < rotationLeftRight)
            {
                rotate = Quaternion.Euler(eulerAngle);
            }
            else
            {
                rotate = Quaternion.Euler(oriRotation.eulerAngles.x, -rotationLeftRight, oriRotation.eulerAngles.z);
            }

            transform.rotation = Quaternion.Slerp(rotate, transform.rotation, speed * Time.deltaTime);
            transform.rotation = Quaternion.Euler(new Vector3(oriRotation.eulerAngles.x, transform.eulerAngles.y, oriRotation.eulerAngles.z));

            //if(Mathf.Abs(transform.eulerAngles.y - 10f) >= rotationLeftRight)
            //    transform.rotation = Quaternion.Euler(transform.eulerAngles.x, -rotationLeftRight, transform.eulerAngles.z);
            //else
            //    transform.rotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y - 10f, transform.eulerAngles.z);
        }
        if (Input.GetButton("CameraRight"))
        {
            eulerAngle.y += 10f;

            if (Mathf.Abs(eulerAngle.y) < rotationLeftRight)
            {
                rotate = Quaternion.Euler(eulerAngle);
            }
            else
            {
                rotate = Quaternion.Euler(oriRotation.eulerAngles.x, rotationLeftRight, oriRotation.eulerAngles.z);
            }

            transform.rotation = Quaternion.Slerp(rotate, transform.rotation, speed * Time.deltaTime);
            transform.rotation = Quaternion.Euler(new Vector3(oriRotation.eulerAngles.x, transform.eulerAngles.y, oriRotation.eulerAngles.z));
            //if (Mathf.Abs(transform.eulerAngles.y + 10f) >= rotationLeftRight)
            //    transform.rotation = Quaternion.Euler(transform.eulerAngles.x, rotationLeftRight, transform.eulerAngles.z);
            //else
            //    transform.rotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y + 10f, transform.eulerAngles.z);
        }
        if (Input.GetButton("CameraUp"))
        {
            eulerAngle = cam.transform.eulerAngles;
            eulerAngle.x -= 10f;

            if (Mathf.Abs(eulerAngle.x) < rotationTop)
            {
                rotate = Quaternion.Euler(eulerAngle);
            }
            else
            {
                rotate = Quaternion.Euler(-rotationTop, camOriRotation.eulerAngles.y, camOriRotation.eulerAngles.z);
            }

            cam.transform.rotation = Quaternion.Slerp(rotate, cam.transform.rotation, speed * Time.deltaTime);
            cam.transform.rotation = Quaternion.Euler(new Vector3(cam.transform.eulerAngles.x, camOriRotation.eulerAngles.y, camOriRotation.eulerAngles.z));

            //if(Mathf.Abs(transform.eulerAngles.x - 10f) >= rotationTop)
            //    transform.rotation = Quaternion.Euler(-rotationTop, transform.eulerAngles.y, transform.eulerAngles.z);
            //else
            //    transform.rotation = Quaternion.Euler(transform.eulerAngles.x - 10f, transform.eulerAngles.y, transform.eulerAngles.z);
        }
        if (Input.GetButton("Reset"))
        {
            transform.rotation = oriRotation;
            cam.transform.rotation = camOriRotation;
        }
            
    }
}
