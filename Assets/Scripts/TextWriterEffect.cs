﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

//https://www.youtube.com/watch?v=U85gbZY6oo8
public class TextWriterEffect : MonoBehaviour
{
    private float delay = 1.0f;
    private TextMeshProUGUI _textMeshPro;
    //public string[] _textCharacter;
    private bool isRunning = false;

    public void Start()
    {
        _textMeshPro = this.GetComponent<TextMeshProUGUI>();
        if (SceneManager.GetActiveScene().buildIndex > 0)
            _textMeshPro.maxVisibleCharacters = 0;
    }

    public void Play()
    {
        if (!isRunning)
            StartCoroutine(TypeWriterEffect());
    }

    IEnumerator TypeWriterEffect()
    {
        //yield return new WaitForSeconds(0.30f);
        //isRunning = true;
        int totalVisibleCharacters = _textMeshPro.textInfo.characterCount;
        int counter = 0;
        int visibleCount = 0;

        while (counter <= totalVisibleCharacters)
        {
            visibleCount = counter % (totalVisibleCharacters + 1);
            _textMeshPro.maxVisibleCharacters = visibleCount;
            counter += 1;
            yield return new WaitForSeconds(0.05f);
        }
        yield return new WaitForSeconds(0.20f);
        _textMeshPro.maxVisibleCharacters = 0;
        isRunning = false;
    }
}
