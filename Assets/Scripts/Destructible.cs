﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{
    public GameObject fractured;
    public GameObject heart;

    void Start()
    {
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }

    //This should be in Game Manager
    public void DestroyCrate()
    {
        var temp = Instantiate(fractured, transform.position, Quaternion.identity);
        if(this.tag != "SmallCrate")
            GenerateHeart();
        Destroy(gameObject);
        Destroy(temp, 2.0f);
    }

    public void GenerateHeart()
    {
        int randomNumber = Random.Range(0, 65536);
        if (randomNumber % 2 == 0)
            Instantiate(heart, GetComponent<Transform>().position, Quaternion.Euler(90, 0, 0));
    }

}
