﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateHealth : MonoBehaviour
{
    public GameObject heart;

    void Create()
    { 
        int randomNumber = Random.Range(0, 65536);
        if (randomNumber % 2 == 0)
            Instantiate(heart, GetComponent<Transform>().position, Quaternion.identity);
    }
}
