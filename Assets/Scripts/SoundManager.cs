﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//https://forum.unity.com/threads/what-is-the-difference-between-soundmanager-and-audiosource.576340/
//Singleton Pattern
public class SoundManager : MonoBehaviour
{
    //public variable
    public static SoundManager Instance = null;

    //private variable
    public float BGMVolume;
    public float SFXVolume;
    [SerializeField] public AudioSource BGM;
    [SerializeField] public AudioSource walk;
    [SerializeField] public AudioSource jump;
    private AudioSource currentAudio;

    //https://forum.unity.com/threads/assigning-sound-file-to-audioclip-by-script-alone-possible.39716/

    public void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (Instance == null)
            Instance = this;
        else if (Instance != this) 
            Destroy(this.gameObject);

        BGMVolume = BGM.volume;
        SFXVolume = walk.volume;
        //audio.loop = true;        
    }

    public void Update()
    {
        switch (SceneManager.GetActiveScene().name)
        {
            case "Home": Play(SoundManager.Instance.BGM); break;
            case "Level1": Stop(SoundManager.Instance.BGM); break;
            case "Level2": Stop(SoundManager.Instance.BGM); break;
        }
    }

    public void Play(AudioSource audioClip)
    {
        if (audioClip.isPlaying) return;
        audioClip.Play();
        currentAudio = audioClip;
        //volume = audioClip.volume;
    }

    public void Stop(AudioSource audioClip)
    {
        audioClip.Stop();
    }

    public void BGMSetVolume(float vol)
    {
        BGMVolume = vol;
        BGM.volume = vol;
    }

    public void SFXSetVolume(float vol)
    {
        SFXVolume = vol;
        walk.volume = vol;
        jump.volume = vol;
    }

    public float BGMGetVolume()
    {
        return BGMVolume;
    }    
    
    public float SFXGetVolume()
    {
        return SFXVolume;
    }

}
