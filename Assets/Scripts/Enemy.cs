﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Reference from: https://www.youtube.com/watch?v=mBGUY7EUxXQ (enemy sight)
/// </summary>
public class Enemy : MonoBehaviour
{
    [Tooltip("Distance between enemy and player, so that the enemy can attack")]
    public float distance;
    public float fieldOfViewAngle;
    public int damage;   
    public Transform[] position;
    public Transform bulletParent;
    public GameObject bullet;
    public GameObject head;
    public bool stationary;

    private GameObject player;
    private Collider foxContactPoint;
    private Rigidbody rigidbody;
    private NavMeshAgent agent;
    private int currentPosition;
    private Animator animator;
    private bool attackState;
    private List<int> movePosition;
    private float fireRate = 500f;
    private float fireCounter = 0;

    public void Start()
    {
        movePosition = new List<int>();
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        currentPosition = 0;
        movePosition.Add(currentPosition);
        attackState = false;
        rigidbody = GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectWithTag("Player");
    }
    
    public void Update()
    {
        if(!stationary)
            Pathfinding();

        //if (playerInSight && Vector3.Distance(player.transform.position, transform.position) <= distance)
        if(CheckInSight())
        {
            animator.SetBool("Attack", true);
            attackState = true;
            rigidbody.constraints = RigidbodyConstraints.FreezeAll; //Freeze the rigidbody to prevent enemy from pushing the player

            if (stationary)
            {
                Vector3 targetDirection = player.transform.position - transform.position;
                Vector3 rotation = Vector3.RotateTowards(transform.forward, targetDirection, 10.0f * Time.deltaTime, 0f);
                transform.rotation = Quaternion.LookRotation(rotation);

                if (Time.time * 1000 > fireCounter)
                {
                    fireCounter = Time.time * 1000 + fireRate;

                    Vector3 position = head.transform.position;

                    var myBullet = Instantiate(bullet, position, bullet.transform.rotation, bulletParent);
                    myBullet.GetComponent<Bullet>().enemyParent = gameObject;
                    myBullet.GetComponent<Bullet>().direction = transform.forward;
                }               
            }                
            
        }
        else
        {
            attackState = false;
            animator.SetBool("Attack", false);
            rigidbody.constraints = RigidbodyConstraints.None;
        }            
    }
    
    private void Pathfinding()
    {
        if (CheckInSight())
        {
            agent.SetDestination(player.transform.position);
        }
        else if (!agent.pathPending && agent.remainingDistance < 0.5f)
        {
            currentPosition = Random.Range(0, position.Length);

            while (movePosition.Contains(currentPosition))
            {
                currentPosition = Random.Range(0, position.Length);
            }            

            movePosition.Add(currentPosition);

            if(movePosition.Count == position.Length)
            {
                movePosition.Clear();
                movePosition.Add(currentPosition);
            }

            agent.SetDestination(position[currentPosition].position);
        }

        //check the enemy is moving
        if (agent.velocity.magnitude > 0)
            animator.SetBool("Move", true);
        else
            animator.SetBool("Move", false);
    }

    private bool CheckInSight()
    {
        float distanceBetween = Vector3.Distance(transform.position, player.transform.position);
        Vector3 direction = player.transform.position - transform.position;
        float angle = Vector3.Angle(direction, transform.forward);

        if (angle < fieldOfViewAngle * 0.5f && distanceBetween <= distance)
        {
            //playerInSight = true;
            return LineOfSight();
        }
        else
            return false;
            //playerInSight = false;
    }

    public void PlayAnimation()
    {        
        animator.SetBool("TakeDamage", true);
    }

    private void OnCollisionEnter(Collision collision)
    {
        var contact = collision.contacts;
        foxContactPoint = player.GetComponent<Player_FSM>().enemyCollisionCheck;
        foreach (var con in contact)
        {
            if (con.otherCollider == foxContactPoint && attackState && !stationary) //if the enemy is attacking player
            {
                GameManager.Instance.ReduceHealth(damage);
                Debug.Log("enemy Attack");
                break;
            }
            else if(con.otherCollider == foxContactPoint && !attackState) //if the enemy is collide with the player but not in attack state
            {
                GameManager.Instance.ReduceHealth(1);
                gameObject.GetComponent<HealthScript>().Hit(1);
                break;
            }
        }
    }

    void OnCollisionExit(Collision collision)
    {
        animator.SetBool("TakeDamage", false);
    }

    public void Die()
    {
        animator.SetBool("Died", true); 
        Destroy(gameObject, 2.0f);
    }

    private bool LineOfSight()
    {
        RaycastHit hitInfo;
        
        if (Physics.Raycast(transform.position, transform.forward, out hitInfo, distance))
        {            
            if (hitInfo.transform.gameObject != player)
            {
                return false;
            }                
        }
        return true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawRay(transform.position, transform.forward);
        //Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
        //Gizmos.DrawWireSphere(tailCollider.center, tailCollider.radius);

    }

}
