﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//https://itnext.io/how-to-write-a-simple-3d-character-controller-in-unity-1a07b954a4ca?gi=6f6e530e71e7
//https://answers.unity.com/questions/334708/gravity-with-character-controller.html
//https://www.raywenderlich.com/6034380-state-pattern-using-unity
public class Player : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float jumpForce;
    public GameObject bullet;
    public Transform bulletParent;
    [Tooltip("Fire rate in Milli seconds")]
    public float fireRate;
    //public GameObject leg;
    public int health;

    //Camera and rotation
    public float camSpeed;

    //Animator
    private Animator animator;

    private float gravity = 9.87f;
    private float fireTimeCounter = 0;
    private Quaternion oriRotation;
    //private Quaternion legOriRotation;
    private bool fire = false;
    private bool canDoubleJump = true;
    private Rigidbody rigidbody;
    private bool isGrounded;

    public void Start()
    {
        oriRotation = transform.rotation;
        //legOriRotation = leg.transform.rotation;
        rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }

    /// <summary>
    /// reference from: https://answers.unity.com/questions/1108797/does-animator-animation-overridesdisables-script-c.html?_ga=2.236763779.309351985.1608290725-1496971421.1608290725
    /// animator overrides the rotation, using late update to fix the problem
    /// </summary>
    public void LateUpdate()
    {
        if (Input.GetButton("Fire1"))
        {
            //leg.transform.rotation = Quaternion.Euler(legOriRotation.eulerAngles.x, legOriRotation.eulerAngles.y, 20f);
            fire = true;
        }
        else if (fire)
        {
            //leg.transform.rotation = legOriRotation;
            fire = false;
        }
    }
    // Update is called once per frame
    public void Update()
    {
    }

    public void FixedUpdate()
    {
        SimpleMovement();
        JumpMovement();
    }

    private void SimpleMovement()
    {
        float horizontalMove = Input.GetAxis("Horizontal");
        float verticalMove = Input.GetAxis("Vertical");
        animator.SetFloat("inputH", horizontalMove);
        animator.SetFloat("inputV", verticalMove);
        Vector3 eulerAngle = transform.eulerAngles;
        Quaternion rotate;

        //reference from: https://forum.unity.com/threads/moving-a-rigidbody-at-a-constant-speed.76470/
        Vector3 move = transform.forward * verticalMove + transform.right * horizontalMove;
        if (rigidbody.velocity.magnitude < speed)
            rigidbody.AddForce(move * speed);
                   
        eulerAngle.y += 1.0f * horizontalMove;
        rotate = Quaternion.Euler(eulerAngle);
        transform.rotation = Quaternion.Lerp(rotate, transform.rotation, camSpeed * Time.deltaTime);
        transform.rotation = Quaternion.Euler(new Vector3(oriRotation.eulerAngles.x, transform.eulerAngles.y, oriRotation.eulerAngles.z));

    }

    private void JumpMovement()
    {
        if (isGrounded)
            canDoubleJump = true;

        if (Input.GetButtonDown("Jump"))
        {
            if (isGrounded)
            {
                rigidbody.velocity = Vector2.up * jumpForce;
                animator.SetTrigger("Jump");
            }
            else {
                if (canDoubleJump)
                {
                    rigidbody.velocity = Vector2.up * jumpForce;
                    animator.SetTrigger("Jump");
                    canDoubleJump = false;
                }                   
            }
        }       
    }

    public void OnCollisionEnter(Collision collision)
    {
        isGrounded = true;
    }

    public void OnCollisionExit(Collision collision)
    {
        isGrounded = false;
    }
}
