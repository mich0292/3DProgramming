# 3DProgramming 🦊🦊 Fox Journey

![Fox Journey](Screenshot/Fox Journey.png "Fox Journey")

*Note: This game uses Unity engine.*

Our game, **Fox Journey** is about a fox going out to the wilderness to find fruits, 🍓🍌🍇🍐🍎. It is a 3D platformer game, where players can walk, jump, double jump and attack. In the course of finding fruits, the player needs to fight against other animals. Platformer is a common genre, so our focus of this game is aesthetics, in which we chose to use *low poly graphics*.

## Controls
  Keys	  |   In-game action
-------   | ------------------------------
   ↑	    |   Move forward
   ↓	    |   Move backward
   ←	    |   Move left
   →	    |   Move right 
  Space   |	 Jump
 Space x2 |	 Double Jump
  Z       |	 Attack


## ❓How to play our game❓
1. Launch Assignment.exe

## Features
###  Player actions
* Walk
* Jump
* Double jump
* Attack  

###  Enemies
- Patrol through the usage of NavMesh
- Use line of sight to determine if player is in sight

###  Audio/Sound effect
- Background music
- Sound effects
- 3D sounds 

### Camera
- Virtual camera + Timeline asset (animating camera)
- Virtual camera following player

### Animation
- Idle animation
- Walk animation
- Jump animation
- Double jump animation
- Attack animation
- Death animation
- Take damage animation

### Collectibles
- Fruits (give scores)
- Health (increase health)

### AI
- Finite state machine (for player movements)
- Line of sight (uses Physics.Raycast)

### Save Game & Load Game

#### Please check out our [documentation](https://github.com/mich0292/Game_Physics/blob/6957b9effd1935083e4262de4c4bd5b76ef51d75/(1171100973_1171101517)%20WrittenReport.pdf) for more details! 😀
